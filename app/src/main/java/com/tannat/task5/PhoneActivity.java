package com.tannat.task5;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PhoneActivity extends Activity {
    private EditText phoneNumberEdt;
    private ImageView callBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act_phone);

        phoneNumberEdt = (EditText) findViewById(R.id.edt_phone_number);
        callBt = (ImageView) findViewById(R.id.iv_call);


        callBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = phoneNumberEdt.getText().toString();
                callPhoneWithNumber(phoneNumber, v);
            }
        });
    }

    private void callPhoneWithNumber(String phoneNumber, View v) {
        String url = "tel:" + phoneNumber;
        Log.i("phone number", url);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
}
