package com.tannat.task5;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.TableRow;

import java.net.URI;

public class ActMainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        // Set click listener
        TableRow trPhone = findViewById(R.id.tr_phone);
        TableRow trPhoto = findViewById(R.id.tr_photo);
        TableRow trCamera = findViewById(R.id.tr_camera);
        TableRow trSms = findViewById(R.id.tr_sms);
        TableRow trFacebook = findViewById(R.id.tr_facebook);

        trPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTrPhone(v);
            }
        });
        trPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTrPhoto(v);
            }
        });
        trCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTrCamera(v);
            }
        });
        trFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTrFacebook(v);
            }
        });
        trSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTrSms(v);
            }
        });
    }

    public void onClickTrPhone(View v){
        Intent intent = new Intent(this, PhoneActivity.class);
        startActivity(intent);
    }

    public void onClickTrPhoto(View v){
        Intent intent = new Intent(Intent.ACTION_VIEW, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivity(intent);
    }

    public void onClickTrCamera(View v){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        startActivity(intent);
    }

    public void onClickTrFacebook(View v){
        String url = "https://www.facebook.com";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    public void onClickTrSms(View v){
        Intent intent = new Intent(this, SendSmsActivity.class);
        startActivity(intent);
    }
}
